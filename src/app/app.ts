import {Component, bootstrap, FORM_DIRECTIVES, CORE_DIRECTIVES, Inject, Injectable} from "angular2/angular2";
import {Hero} from './hero'
import {HeroesService} from './heroes-service'

@Component({
    selector: "my-app",
    templateUrl: "templates/hero-editor-tpl.html",
    directives: [FORM_DIRECTIVES, CORE_DIRECTIVES],
    providers: [HeroesService]
})
class AppComponent {
    title = "Tour of Heroes";
    heroes:Hero[];
    selectedHero:Hero;

    constructor(@Inject(HeroesService) heroesService) {
        heroesService.heroes.subscribe(function (res) {
            console.log(res);
        });
    }

    onSelect(hero:Hero) {
        this.selectedHero = hero;
    }

    getSelectedClass(hero:Hero) {
        return {
            selected: hero === this.selectedHero
        }
    }
}

bootstrap(AppComponent, [HeroesService]);
