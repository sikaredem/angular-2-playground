import {Injectable} from "angular2/angular2"
import {Hero} from "./hero"
import {Http, HTTP_PROVIDERS} from "angular2/http"
import {API_BASE_URL} from "./config"

@Injectable()
export class HeroesService {
    http: Http;

    constructor(http: Http) {
        this.http = http
    }

    retrieveHeroes() {
        return this.http.get(API_BASE_URL + '/hero');
    }
}
