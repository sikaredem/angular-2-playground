var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var angular2_1 = require("angular2/angular2");
var heroes_service_1 = require('./heroes-service');
var AppComponent = (function () {
    function AppComponent(heroesService) {
        this.title = "Tour of Heroes";
        heroesService.heroes.subscribe(function (res) {
            console.log(res);
        });
    }
    AppComponent.prototype.onSelect = function (hero) {
        this.selectedHero = hero;
    };
    AppComponent.prototype.getSelectedClass = function (hero) {
        return {
            selected: hero === this.selectedHero
        };
    };
    AppComponent = __decorate([
        angular2_1.Component({
            selector: "my-app",
            templateUrl: "templates/hero-editor-tpl.html",
            directives: [angular2_1.FORM_DIRECTIVES, angular2_1.CORE_DIRECTIVES],
            providers: [heroes_service_1.HeroesService]
        }),
        __param(0, angular2_1.Inject(heroes_service_1.HeroesService)), 
        __metadata('design:paramtypes', [Object])
    ], AppComponent);
    return AppComponent;
})();
angular2_1.bootstrap(AppComponent, [heroes_service_1.HeroesService]);
//# sourceMappingURL=app.js.map