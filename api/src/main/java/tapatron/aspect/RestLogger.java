package tapatron.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
public class RestLogger {

    @Before("execution(* tapatron.controller.HeroController.heroes(..))")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("logBefore() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("hijacked : " + Arrays.toString(joinPoint.getArgs()));
        System.out.println("******");
    }
}