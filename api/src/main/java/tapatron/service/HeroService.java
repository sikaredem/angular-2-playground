package tapatron.service;

import org.springframework.stereotype.Service;
import tapatron.domain.Hero;

import java.util.Arrays;
import java.util.List;

@Service
public class HeroService {
    List<Hero> heroes = Arrays.asList(
            new Hero("Mr. Nice", 1),
            new Hero("Narco", 2),
            new Hero("Bombasto", 3),
            new Hero("Celeritas", 4),
            new Hero("RubberMan", 5),
            new Hero("Magma", 6),
            new Hero("Tornado", 7));

    public List<Hero> findAll() {
        return heroes;
    }
}
